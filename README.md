# Vorticity

Mapping the vorticity field with spectroscopic galaxies

## References

- 0001041
- 9812456
- Marinoni C., Buzzi A., 2010, Nature, 468, 539
- 1108.0932
- 1108.0932
- 1204.5761
- 1309.7905
- 1709.10378
- 2003.04800
- 2204.0119

## Data:

- BOSS DR12 (CMASS or LOWZ): https://data.sdss.org/sas/dr12/boss/lss/
- eBOSS DR16 LRG: https://data.sdss.org/sas/dr16/eboss/lss/catalogs/DR16/

## Work plan:

1. Download the catalogs
1. Plot redshift distributions and check the mean redshift of these galaxy samples
1. Find an estimate for $\Delta r$
1. Read references and decide what criterion you will use to select the paired galaxies
1. Write a code to generate a map of the vorticity field. Actually, you will have two maps, one per component. 
1. Find the angular correlation ($C_\ell$) of this map. Problem: what's the noise? Like for shear?
1. Implement a mapper in [xCell](https://github.com/xC-ell/xCell) and find the angular correlation of the vorticity field with some galaxy clustering sample (e.g. DESI Legacy Survey). Possible problem: will there be correlations between $z_c$ and $\delta$?
1. What theoretical model can we use to fit this data?
1. If any, fit it.
1. Write the paper

## Paper:

- In Overleaf

## TODO:
 - Check covariance with jacknife
 - Check vorticity maps. There might be some bug.
