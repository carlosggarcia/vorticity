from pairing import PairsBase

class PairsRelativeIsolated(PairsBase):
    def __init__(self, cat, radius_cut, closer_next_gal=2.5):
        self._get_defaults(cat)
        self.radius_cut = radius_cut
        self.closer_next_gal = closer_next_gal

    def _get_pairs(self):
        if self.pairs is None:
            # From arXiv:1611.02618
            train_sample = self.get_training_sample()
            bt = self._get_ball_tree()
            # Select pairs of galaxies that are mutually nearest neighbors
            # and whose next nearest galaxy is 2.5 times further.
            # query returns the results sorted by distance, with the first element being itself.
            nn_dist, nn_ixs = bt.query(train_sample, 3)
            sel = nn_dist[:, 1] < self.radius_cut
            sel *= self.closer_next_gal * nn_dist[:, 1] < nn_dist[:, 2]

            self.isolated = self._filter_mutually_closest_pairs(nn_ixs[:, :2],
                                                               nn_dist[:, :2],
                                                               nn_ixs[sel, :2])

        return self.isolated
