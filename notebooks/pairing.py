import fitsio
import numpy as np
import pyccl as ccl
import healpy as hp
from sklearn.neighbors import NearestNeighbors, BallTree

# Use a class to avoid repeating
class PairsBase():
    def __init__(self, cat):
        self._get_defaults(cat)

    def _get_defaults(self, cat):
        if isinstance(cat, str):
            cat = fitsio.read(cat)
        elif isinstance(cat, list):
            cat_arr = []
            for cf in cat:
                cat_arr.append(fitsio.read(cf))
            cat = np.concatenate(cat_arr)

        # Clean it from galaxies with unknown redshift or z < 0
        z = cat['Z']
        self.cat =  cat[~np.isnan(z) * z>=0]

        # This will have the galaxies coordinates
        self.train_sample = None
        self.ball_tree = None

        # Dictionary with useful information of the paired galaxies
        self.pairs = None

        # Dictinary of maps
        self.maps = {}

        # Cosmolgy
        self.cosmo = ccl.CosmologyVanillaLCDM()
        self.h = 0.67  # TODO: Check how to get this from self.cosmo
        self.c = 3e5  # Speed of light

    def _get_pairs(self):
        raise NotImplementedError('Do not use this base class')

    def get_pairs(self):
        if self.pairs is None:
            self.pairs = self._get_pairs()

        return self.pairs

    def _get_ball_tree(self):
        if self.ball_tree is None:
            train_sample = self.get_training_sample()
            self.ball_tree = BallTree(train_sample)
        return self.ball_tree

    def _filter_mutually_closest_pairs(self, nn_ixs, nn_dist, nn_ixs_cut=None):
        pairs_ixs = []
        pairs_dist = []
        if nn_ixs_cut is None:
            nn_ixs_cut = nn_ixs

        for i1, i2 in nn_ixs_cut:
            if ((i1, i2) in pairs_ixs) or ((i2, i1) in pairs_ixs):
                continue
            if nn_ixs[i2][1] == i1:
                pairs_ixs.append((i1, i2))
                pairs_dist.append(nn_dist[i1][1])
        pairs_ixs = np.array(pairs_ixs)
        pairs_dist = np.array(pairs_dist)
        npairs = pairs_dist.size

        return {'ixs': pairs_ixs, 'dist': pairs_dist, 'npairs': npairs}

    def get_training_sample(self):
        if self.train_sample is None:
            cat = self.cat
            z = cat['Z']
            r = ccl.comoving_angular_distance(self.cosmo,
                                              1 / (1 + z)) / (1 + z)

            # Go to Cartesian coordinates (x, y, z)
            # We need to convert angles to radians
            theta = (90 - cat['DEC']) * np.pi / 180
            phi = cat['RA'] * np.pi / 180
            train_sample = np.array([r * np.sin(theta) * np.cos(phi),
                                     r * np.sin(theta) * np.sin(phi),
                                     r * np.cos(theta)]).T

            self.train_sample = train_sample

        return self.train_sample

    def get_cm_coords(self):
        pairs = self.get_pairs()
        if 'cm' not in pairs:
            train_sample = self.get_training_sample()
            pairs_ixs = pairs['ixs']
            c1 = train_sample[pairs_ixs[:, 0]]
            c2 = train_sample[pairs_ixs[:, 1]]
            pairs['cm' ] = (c1 + c2) / 2
        return pairs['cm']

    def get_dz(self):
        pairs = self.get_pairs()
        if 'dz' not in pairs:
            z = self.cat['Z']
            pairs_ixs = pairs['ixs']
            z1 = z[pairs_ixs[:, 0]]
            z2 = z[pairs_ixs[:, 1]]

            pairs['dz' ] = z1 - z2

        return pairs['dz']

    def get_ds(self):
        pairs = self.get_pairs()
        if 'ds' not in pairs:
            train_sample = self.get_training_sample()
            pairs_ixs = pairs['ixs']

            c1 = train_sample[pairs_ixs[:, 0]]
            c2 = train_sample[pairs_ixs[:, 1]]

            pairs['ds' ] = c1 - c2

        return pairs['ds']

    def get_ds_tangent_plane(self):
        pairs = self.get_pairs()
        if 'ds_tangent_plane' not in pairs:
            cat = self.cat
            train_sample = self.get_training_sample()
            pairs_ixs = pairs['ixs']

            cm = self.get_cm_coords()
            r_cm = np.sqrt(np.sum(cm))

            ds2 = np.sum(self.get_ds()**2)
            phi = np.radians(cat['RA'])
            theta = np.radians(90 - cat['DEC'])

            dphi = phi[pairs_ixs[:, 0]] - phi[pairs_ixs[:, 1]]
            dtheta = theta[pairs_ixs[:, 0]] - theta[pairs_ixs[:, 1]]

            rphi = dphi * r_cm * np.sin(hp.vec2ang(cm))
            rtheta = -dtheta * r_cm

            pairs['ds_tangent_plane'] = np.array([rphi, rtheta])

        return pairs['ds_tangent_plane']

    def get_vort(self):
        pairs = self.get_pairs()
        if 'vort' not in pairs:
            dz = self.get_dz()
            ds_tp = self.get_ds_tangent_plane()

            # vort = dz / r^2 * (r_phi u_theta - r_theta u_phi)
            vort1 =  ds_tp[:, 0]
            vort2 = -ds_tp[:, 1]

            pairs['vort'] = dz/pairs['dist']**2 * np.array([vort1, vort2])

        return pairs['vort']

    def get_vorticity_map(self, nside, to_plot=False):
        if 'vort' not in self.maps:
            cm = self.get_cm_coords()
            vort = self.get_vort()
            npix = hp.nside2npix(nside)
            ipix = hp.vec2pix(nside, cm[:, 0], cm[:, 1], cm[:, 2])
            nc_map = np.bincount(ipix, minlength=npix)
            vort_map1 = np.bincount(ipix, weights=vort[0], minlength=npix)
            vort_map2 = np.bincount(ipix, weights=vort[1], minlength=npix)
            goodpix = nc_map > 0
            vort_map1[goodpix] /= nc_map[goodpix]
            vort_map2[goodpix] /= nc_map[goodpix]

            self.maps['vort'] = np.array([vort_map1, vort_map2])

        if to_plot:
            nc = self.get_number_counts_map(nside)
            goodpix = nc > 0
            vort_map = self.maps['vort'].copy()
            vort_map[:, ~goodpix] = hp.UNSEEN
            return vort_map

        return self.maps['vort']

    def get_number_counts_map(self, nside):
        if 'nc' not in self.maps:
            cm = self.get_cm_coords()
            npix = hp.nside2npix(nside)
            ipix = hp.vec2pix(nside, cm[:, 0], cm[:, 1], cm[:, 2])
            self.maps['nc'] = np.bincount(ipix, minlength=npix)

        return self.maps['nc']
