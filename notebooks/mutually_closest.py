from pairing import PairsBase

class PairsMutuallyClosest(PairsBase):
    def __init__(self, cat, radius_cut):
        self._get_defaults(cat)
        self.radius_cut = radius_cut

    def _get_pairs(self):
        if self.pairs is None:
            train_sample = self.get_training_sample()
            bt = self._get_ball_tree()
            # Select pairs of galaxies (k = 2) that are mutually nearest neighbors
            nn_dist, nn_ixs = bt.query(train_sample, 2)
            sel = nn_dist[:, 1] < self.radius_cut

            self.pairs = self._filter_mutually_closest_pairs(nn_ixs, nn_dist,
                                                             nn_ixs[sel])
        return self.pairs

