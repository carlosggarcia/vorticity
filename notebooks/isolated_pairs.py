import numpy as np
from pairing import PairsBase

class PairsIsolated(PairsBase):
    def __init__(self, cat, radius_cut):
        self._get_defaults(cat)
        self.radius_cut = radius_cut

    def _get_pairs(self):
        if self.pairs is None:
            # From arXiv: 1204.5761
            train_sample = self.get_training_sample()
            bt = self._get_ball_tree()
            # Select pairs of galaxies (k = 2) that are mutually nearest neighbors
            nn_ixs, nn_dist = bt.query_radius(train_sample, self.radius_cut,
                                              return_distance=True)

            pairs_ixs = []
            pairs_dist = []
            for ixs, dists in zip(nn_ixs, nn_dist):
                if ixs.size == 2:
                    pairs_ixs.append(ixs)
                    pairs_dist.append(dists)
            pairs_ixs = np.array(pairs_ixs)
            pairs_dist = np.array(pairs_dist)

            self.pairs = self._filter_mutually_closest_pairs(nn_ixs, nn_dist,
                                                             pairs_ixs)

        return self.pairs
